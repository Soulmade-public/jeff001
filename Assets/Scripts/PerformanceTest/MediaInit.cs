﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using monoflow;

public class MediaInit : MonoBehaviour
{
    [SerializeField] GameObject mpPrefab;
    [SerializeField] Shader shader;

    List<Media> mediaList = new List<Media>();
    List<MPMP> mpList = new List<MPMP>();
    List<GameObject> goList = new List<GameObject>();

    int sceneIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        //If looping == true then PlaybackCompleted will not be recognised

        mediaList.Add(new Media(0, 0, "Overgang2k_Black.mp4", true, false));
        //mediaList.Add(new Media(0, 1, "bed2k.mp4", true, false));
        //mediaList.Add(new Media(0, 2, "Achtergrond2k.mp4", true, false));
        //mediaList.Add(new Media(2, 0, "360_Video_2 kopie.mp4", true, false));

        

        for (int i = 0; i < mediaList.Count; i++)
        {
            goList.Add(GameObject.Instantiate(mpPrefab));

            //goList.Last().transform.localScale =
            //    new Vector3(goList.Last().transform.localScale.x + mediaList[i].order,
            //    goList.Last().transform.localScale.y + mediaList[i].order,
            //    goList.Last().transform.localScale.z + mediaList[i].order);

            Material mat = new Material(shader);
            goList[i].GetComponent<Renderer>().material = mat;
            //goList[i].SetActive(false);
            goList[i].GetComponent<Renderer>().enabled = false;

            MPMP mpmp = goList[i].GetComponentInChildren<MPMP>();
            mpmp.SetVideoMaterial(mat);
            mpmp.looping = mediaList[i].isLooping;
            mpmp.Load(mediaList[i].path);
            mpList.Add(mpmp);
        }
        //Debug.Log(mpList.Last().videoPath);
        //mpList.Last().OnLoaded += Initialize;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PlayMedia (MPMP mp)
    {
        Debug.Log("Now playing: " + sceneIndex);

        for (int i = 0; i < mediaList.Count; i++)
        {
            goList[i].GetComponent<Renderer>().enabled = false;

            if (mediaList[i].scene == sceneIndex)
            {
                goList[i].GetComponent<Renderer>().enabled = true;
                mpList[i].Play();

                Debug.Log("Playing: " + mediaList[i].path);

                if (mediaList[i].isTrigger)
                {
                    mpList[i].OnPlaybackCompleted += PlayMedia;
                    mpList[i].OnPlaybackCompleted += PlaybackCompletedDebug;
                }
            }
        }

        sceneIndex++;
    }

    public void Initialize()
    {
        Debug.Log("Initialize");
        PlayMedia(mpList.First());

    }

    void PlaybackCompletedDebug (MPMP mp)
    {
        Debug.Log("Playback completed");
    }
}
