﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Media : MonoBehaviour
{
    public int scene;
    public int order;
    public string path;
    public bool isLooping;
    public bool isTrigger;

    public Media(int Scene, int Order, string Path, bool IsLooping, bool IsTrigger)
    {
        scene = Scene;
        order = Order;
        path = Path;
        isLooping = IsLooping;
        isTrigger = IsTrigger;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
