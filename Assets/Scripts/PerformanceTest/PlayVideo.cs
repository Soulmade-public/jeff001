﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using monoflow;

public class PlayVideo : MonoBehaviour
{
    public MPMP mediaPlayer;
    public Material mpMat;

    // Start is called before the first frame update
    void Start()
    {
        mediaPlayer.looping = true;
        mediaPlayer.Load("360Video.mp4");
        mediaPlayer.SetVideoMaterial(mpMat);
        mediaPlayer.OnLoaded += Playback;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            Debug.Log("Play");
            Playback(mediaPlayer);
        }
    }

    void Playback (MPMP mp)
    {
        mp.Play();
    }
}
