﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationLayer : MonoBehaviour
{
    public AnimationLayer()
    {
        description = "";
        videoPath = "";
        isIdle = false;
        isTrigger = false;
        hitboxRotation = Vector3.zero;
    }

    public AnimationLayer(string Description, string Path, bool Idle, bool Trigger, Vector3 HitboxRotation)
    {
        description = Description;
        videoPath = Path;
        isIdle = Idle;
        isTrigger = Trigger;
        hitboxRotation = HitboxRotation;
    }

    public string description;
    public string videoPath;
    public bool isIdle;
    public bool isTrigger;
    public Vector3 hitboxRotation;



}
