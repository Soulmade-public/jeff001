﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using monoflow;

public class GazeController : MonoBehaviour

{

    [SerializeField] GameObject playerCamera;
    [SerializeField] int raycastLength = 20;
    [SerializeField] GameObject GO;

    bool isTriggered = false;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        

        GazeRaycast();

        //Debug.DrawRay(playerCamera.transform.position, playerCamera.transform.forward * 20, Color.blue, 5);
    }

    void GazeRaycast()
    {
        Ray ray = new Ray(playerCamera.transform.position, playerCamera.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.tag == "Gaze")
            {
                if (!isTriggered)
                {
                    isTriggered = true;
                    GO.GetComponent<MediaInit>().Initialize();
                }
            }
        }
    }
}
