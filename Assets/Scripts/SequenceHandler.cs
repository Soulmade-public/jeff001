﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using monoflow;

public class SequenceHandler : MonoBehaviour
{
    int shotIndex = 0;
    [SerializeField] MPMP [] videoLayers;

    private void Start()
    {
        SetupVideoPlayers(shotIndex);
        SetupLayers(shotIndex);

        for (int i = 0; i < videoLayers.Length; i++)
        {
            videoLayers[i].OnLoaded += PlaybackVideo;
            //NIET VERGETEN TE UNSUBSCRIBEN, MEMORY LEAKS!!!
        }
    }

    void PlaybackVideo(MPMP mp)
    {
        mp.Play();
    }

    void SetupLayers(int index)
    {
        for (int i = 0; i < SequenceManager.animatieArray.GetLength(0); i++)
        {
            videoLayers[i].looping = true;

            if (SequenceManager.animatieArray[i, index].description != "")
            {
                videoLayers[i].transform.parent.gameObject.SetActive(true);
                videoLayers[i].looping = true;
                Debug.Log("loopng ON");
                videoLayers[i].Load(SequenceManager.animatieArray[i, index].videoPath);

                if (SequenceManager.animatieArray[i, index].isTrigger)
                {
                    //Load next shots after this animation finishes
                    videoLayers[i].looping = false;
                    Debug.Log("loopng OFF");
                    videoLayers[i].OnPlaybackCompleted += NextShot;
                }

                else if (SequenceManager.animatieArray[i, index].hitboxRotation != Vector3.zero)
                {
                    //videoLayers[i].looping = true;
                    Debug.Log("Disable autoplay");
                    

                    //Turn off autoplay
                    for (int j = 0; j < videoLayers.Length; j++)
                    {
                        videoLayers[j].OnLoaded -= PlaybackVideo;
                    }

                    //Reposition and activate the look at hitbox
                    videoLayers[i].transform.GetChild(0).gameObject.SetActive(true);
                    videoLayers[i].transform.GetChild(0).rotation = Quaternion.Euler(SequenceManager.animatieArray[i, index].hitboxRotation);
                }
            }

            else
            {
                videoLayers[i].transform.parent.gameObject.SetActive(false);
            }
        }
    }

    void SetupVideoPlayers (int index)
    {
        //for (int i = 0; i < videoLayers.Length; i++)
        //{
        //    videoLayers[i].OnLoaded += PlaybackVideo;
        //    //NIET VERGETEN TE UNSUBSCRIBEN, MEMORY LEAKS!!!
        //}
    }

    void NextShot (MPMP mp)
    {
        //Debug.Log("Next Shot");
        shotIndex++;
        //mp.OnLoaded -= NextShot;
        SetupLayers(shotIndex);

        //for (int i = 0; i < videoLayers.Length; i++)
        //{
        //    Debug.Log("clear subscriptionNextShot - index: " + shotIndex);
        //    videoLayers[i].OnLoaded -= NextShot;
        //}
    }
}
