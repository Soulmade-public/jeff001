﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SequenceManager : MonoBehaviour
{
    public static AnimationLayer [,] animatieArray;
    int arrayRows = 4;
    int arrayColumns = 12;

    // Start is called before the first frame update
    void Start()
    {
        animatieArray = new AnimationLayer[arrayRows, arrayColumns];

        string textLog = " ";

        animatieArray[0, 0] = new AnimationLayer("Background anim", "Achtergrond.mp4", true, false,Vector3.zero);
        animatieArray[0, 1] = new AnimationLayer();
        animatieArray[0, 2] = new AnimationLayer();
        animatieArray[0, 3] = new AnimationLayer();
        animatieArray[0, 4] = new AnimationLayer();
        animatieArray[0, 5] = new AnimationLayer();
        animatieArray[0, 6] = new AnimationLayer();
        animatieArray[0, 7] = new AnimationLayer();
        animatieArray[0, 8] = new AnimationLayer();
        animatieArray[0, 9] = new AnimationLayer();
        animatieArray[0, 10] = new AnimationLayer();
        animatieArray[0, 11] = new AnimationLayer();

        animatieArray[1, 0] = new AnimationLayer("Background anim", "Achtergrond.mp4", true, true, Vector3.zero);
        animatieArray[1, 1] = new AnimationLayer();
        animatieArray[1, 2] = new AnimationLayer();
        animatieArray[1, 3] = new AnimationLayer();
        animatieArray[1, 4] = new AnimationLayer();
        animatieArray[1, 5] = new AnimationLayer();
        animatieArray[1, 6] = new AnimationLayer();
        animatieArray[1, 7] = new AnimationLayer();
        animatieArray[1, 8] = new AnimationLayer();
        animatieArray[1, 9] = new AnimationLayer();
        animatieArray[1, 10] = new AnimationLayer();
        animatieArray[1, 11] = new AnimationLayer();

        animatieArray[2, 0] = new AnimationLayer();
        animatieArray[2, 1] = new AnimationLayer();
        animatieArray[2, 2] = new AnimationLayer();
        animatieArray[2, 3] = new AnimationLayer();
        animatieArray[2, 4] = new AnimationLayer();
        animatieArray[2, 5] = new AnimationLayer();
        animatieArray[2, 6] = new AnimationLayer();
        animatieArray[2, 7] = new AnimationLayer();
        animatieArray[2, 8] = new AnimationLayer();
        animatieArray[2, 9] = new AnimationLayer();
        animatieArray[2, 10] = new AnimationLayer();
        animatieArray[2, 11] = new AnimationLayer();

        animatieArray[3, 0] = new AnimationLayer();
        animatieArray[3, 1] = new AnimationLayer();
        animatieArray[3, 2] = new AnimationLayer();
        animatieArray[3, 3] = new AnimationLayer();
        animatieArray[3, 4] = new AnimationLayer();
        animatieArray[3, 5] = new AnimationLayer();
        animatieArray[3, 6] = new AnimationLayer();
        animatieArray[3, 7] = new AnimationLayer();
        animatieArray[3, 8] = new AnimationLayer();
        animatieArray[3, 9] = new AnimationLayer();
        animatieArray[3, 10] = new AnimationLayer();
        animatieArray[3, 11] = new AnimationLayer();

        foreach (AnimationLayer item in animatieArray)
        {
            if (item.description != "")
            {
                //Debug.Log(item.description);
            }
        }
    }
}
